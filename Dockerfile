FROM compulim/msbuild
ADD . /build
WORKDIR /build
## Build binaries in msbuild container
RUN nuget restore
RUN msbuild .\LibBoggle\LibBoggle.csproj /t:Build /p:Configuration=Release /p:TargetFramework=v4.6.1
RUN msbuild .\Boggle\Boggle.csproj /t:Build /p:Configuration=Release /p:TargetFramework=v4.6.1

## Stage 2, copy binary to iis root
FROM microsoft/aspnet:4.7.1-windowsservercore-1709
COPY --from=0 /build/Boggle /inetpub/wwwroot

## Entrypoint already setup from aspnet container

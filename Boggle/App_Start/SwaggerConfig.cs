using Swashbuckle.Application;
using Swashbuckle.Examples;
using System.Web.Http;
using WebActivatorEx;

using Boggle;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Boggle
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Boggle");
                    c.OperationFilter<ExamplesOperationFilter>();
                })
                .EnableSwaggerUi(c =>
                    {
                        c.DocumentTitle("Boggle Swagger UI");
                        c.DocExpansion(DocExpansion.List);
                    });
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LibBoggle.Data;

namespace Boggle.Controllers
{
    public class WordController : ApiController
    {
        private readonly IBoggleDictionary _boggleDictionary;

        public WordController(IBoggleDictionary boggleDictionary)
        {
            _boggleDictionary = boggleDictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/words/{word}")]
        [ResponseType(typeof(List<DictionaryEntry>))]


        public async Task<HttpResponseMessage> GetEntries(string word)
        {
            var resp = await _boggleDictionary.GetEntries(word);

            if (!resp.Any())
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error: '" + word + "' not found in dictionary.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp.ToList());
        }

        /// <summary>
        /// Checks a word for 
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/words")]
        [ResponseType(typeof(List<DictionaryEntry>))]


        public async Task<HttpResponseMessage> GetEntriesByPrefix(string prefix)
        {
            var resp = await _boggleDictionary.GetEntriesByPrefix(prefix);

            if (!resp.Any())
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error: no words starting with'" + prefix + "' not found in dictionary.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp.ToList());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/words/{word}/exists")]
        [ResponseType(typeof(bool))]


        public async Task<HttpResponseMessage> IsWord(string word)
        {
            return Request.CreateResponse(HttpStatusCode.OK, await _boggleDictionary.IsWord(word));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/words/exists")]
        [ResponseType(typeof(bool))]


        public async Task<HttpResponseMessage> IsPotentialWord(string prefix)
        {
            return Request.CreateResponse(HttpStatusCode.OK, await _boggleDictionary.IsPotentialWord(prefix));
        }
    }
}

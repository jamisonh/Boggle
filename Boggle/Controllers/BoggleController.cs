﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Boggle.Models;
using LibBoggle;
using LibBoggle.Data;
using Swashbuckle.Examples;

namespace Boggle.Controllers
{
    public class BoggleController : ApiController
    {
        private readonly IBoggleDictionary _boggleDictionary;

        public BoggleController(IBoggleDictionary boggleDictionary)
        {
            _boggleDictionary = boggleDictionary;
        }


        /// <summary>
        /// Generates a random board, and then finds words on that board.
        /// </summary>
        /// <param name="boardSize">The size of the board to generate</param>
        /// <param name="minWordLength">The minimum length to be considered a word (defaults to 4)</param>
        /// <param name="concurrent">Whether to run a concurrent scan on the board for words</param>
        /// <returns>The board, any words found and some metrics about the words that were discovered.</returns>
        [HttpGet]
        [Route("api/boggle/generate")]
        [ResponseType(typeof(BoggleBoardModel))]
        public async Task<HttpResponseMessage> GenerateBoardAndFindWords(uint boardSize, uint minWordLength = 4, 
            bool concurrent = false)
        {
            return Request.CreateResponse(HttpStatusCode.OK,
                await CalcFromBoard(new BoggleBoard(boardSize), minWordLength, concurrent));
        }

        /// <summary>
        /// Takes an input boggle board, searches for words on the board.
        /// </summary>
        /// <param name="boggleBoardModel">The JSON body expected </param>
        /// <param name="minWordLength">The minimum length to be considered a word (defaults to 4)</param>
        /// <param name="concurrent">Whether to run a concurrent scan on the board for words</param>
        /// <returns>The input board, any words found and some metrics about the words that were discovered.</returns>
        [HttpPost]
        [Route("api/boggle")]
        [ResponseType(typeof(BoggleBoardModel))]
        [SwaggerRequestExample(typeof(BoggleBoardModel), typeof(BoggleBoardModelExample))]
        public async Task<HttpResponseMessage> FindWords([FromBody]BoggleBoardModel boggleBoardModel, uint minWordLength = 4,
            bool concurrent = false)
        {
            var convertedBoard = BoggleBoardModel.StringBoardToCharBoard(boggleBoardModel.Board);
            return Request.CreateResponse(HttpStatusCode.OK,
                await CalcFromBoard(BoggleBoard.FromBoard(convertedBoard), minWordLength, concurrent));
        }

        public async Task<BoggleBoardModel> CalcFromBoard(BoggleBoard board, uint minWordLength, bool concurrent)
        {
            var words = (await BoggleWordFinder.GetWords(board, _boggleDictionary, minWordLength, concurrent)).ToList();
            return new BoggleBoardModel(board, words);
        }
    }
}

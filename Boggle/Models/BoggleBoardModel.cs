﻿using System.Collections.Generic;
using System.Runtime.Serialization;

using LibBoggle;
using Swashbuckle.Examples;

namespace Boggle.Models
{
    [DataContract]
    public class BoggleBoardModel
    {
        /// <summary>
        /// This is a string array rather than a char array, because .net Deserialization of empty 
        /// strings into a char array result in a null array.
        /// The string array preserves empty strings, so we can message user a specific row/column that
        /// was passed in as an empty.
        /// </summary>
        [DataMember(IsRequired = true)]
        public string[,] Board { get; set; }

        [DataMember(IsRequired = false)]
        public Dictionary<int, int> WordMetrics { get; set; }
        
        [DataMember(IsRequired = false)]
        public int TotalWords { get; set; }

        [DataMember(IsRequired = false)]
        public List<string> WordsFound { get; set; }

        public BoggleBoardModel(){}

        public BoggleBoardModel(BoggleBoard boggleBoard, List<string>  wordsFound)
        {
            Board = CharBoardToStringBoard(boggleBoard.Board);
            WordsFound = wordsFound;
            CalcLengthMetrics();
        }

        /// <summary>
        /// Calculates metrics for a given 
        /// </summary>
        private void CalcLengthMetrics()
        {
            var metrics = new Dictionary<int, int>();
            foreach (var word in WordsFound)
            {
                TotalWords++;
                var length = word.Length;
                if (!metrics.ContainsKey(length))
                {
                    metrics.Add(length, 1);
                }
                else
                {
                    metrics[length] = ++metrics[length];
                }
            }

            WordMetrics = metrics;
        }

        /// <summary>
        /// Supports conversion to the BoggleBoard contract for a char array allows deserialization
        /// </summary>
        /// <param name="stringBoard"></param>
        /// <returns></returns>
        public static char[,] StringBoardToCharBoard(string[,] stringBoard)
        {
            var charBoard = new char[stringBoard.GetLength(0), stringBoard.GetLength(1)];

            for (var row = 0; row < stringBoard.GetLength(0); row++)
            {
                for (var col = 0; col < stringBoard.GetLength(1); col++)
                {
                    var str = stringBoard[row, col];
                    if (str == string.Empty)
                        charBoard[row, col] = BoggleBoard.Empty;
                    else
                        charBoard[row, col] = str.ToCharArray()[0];
                }
            }
            
            return charBoard;
        }

        private static string[,] CharBoardToStringBoard(char[,] charboard)
        {
            var stringBoard = new string[charboard.GetLength(0), charboard.GetLength(1)];

            for (var row = 0; row < charboard.GetLength(0); row++)
            {
                for (var col = 0; col < charboard.GetLength(1); col++)
                {
                    stringBoard[row, col] = charboard[row, col].ToString();
                }
            }

            return stringBoard;
        }
    }


    public class BoggleBoardModelExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new BoggleBoardModel
            {
                Board = new string[,] {{"y", "o", "x"}, {"r", "b", "a"}, {"v", "e", "d"}}
            };
        }
    }
}
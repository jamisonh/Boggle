﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using LibBoggle;
using LibBoggle.Data;

namespace Boggle.Tests
{
    [TestClass]
    public class BoggleWordFinderTests
    {
        [TestMethod]
        public void FindWordsTest()
        {
            var expectedWords = new List<string>
            {
                "bred",
                "yore",
                "byre",
                "abed",
                "oread",
                "bore",
                "orby",
                "robed",
                "broad",
                "byroad",
                "robe",
                "bored",
                "derby",
                "bade",
                "aero",
                "read",
                "orbed",
                "verb",
                "aery",
                "bead",
                "bread",
                "very",
                "road"
            };

            var mockDictionary = new Mock<IBoggleDictionary>();
            mockDictionary.Setup(x => x.IsWord(It.IsIn<string>(expectedWords))).Returns(Task.FromResult(true));
            mockDictionary.Setup(x => x.IsWord(It.IsNotIn<string>(expectedWords))).Returns(Task.FromResult(false));
            mockDictionary.Setup(x => x.IsPotentialWord(It.IsAny<string>())).Returns(Task.FromResult(true));

            var board = BoggleBoard.FromBoard(new char[,]
            {
                {'y', 'o', 'x'}, {'r', 'b', 'a'}, {'v','e','d'}
            });

            var foundWords = BoggleWordFinder.GetWords(board, mockDictionary.Object, 4).Result;

            Assert.IsTrue(foundWords.Count >= expectedWords.Count);
            Assert.IsTrue(expectedWords.All(foundWords.Contains));
        }
        
    }
}
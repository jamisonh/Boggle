﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using LibBoggle;

namespace Boggle.Tests
{
    [TestClass]
    public class BoggleBoardTest
    {
        [TestMethod]
        public void TestBoggleBoardConstructor()
        {
            const int size = 7;
            var boggle = new BoggleBoard(size);

            Assert.IsTrue(boggle.Board.IsFixedSize);
            Assert.AreEqual(boggle.Board.Rank, 2);
            Assert.AreEqual(boggle.Board.Length, size * size);

            foreach (var c in boggle.Board)
            {
                Assert.AreNotEqual(new char(), c);
                Assert.AreNotEqual(BoggleBoard.Empty,c );
            }
        }

        [TestMethod]
        public void TestBoundaryAlphabet()
        {
            var aCount = 0;
            var zCount = 0;

            var loopCount = 10;
            while (aCount == 0 || zCount == 0)
            {
                if (loopCount > 10)
                    break;
                var boggle = new BoggleBoard(100);

                foreach (var c in boggle.Board)
                {
                    if (c == 'a')
                        aCount++;
                    else if (c == 'z')
                    {
                        zCount++;
                    }
                }
            }

            Assert.IsTrue(aCount > 0);
            Assert.IsTrue(zCount > 0);
        }

        [TestMethod]
        public void TestAt()
        {
            var b = new BoggleBoard(10);
            var board = b.Board;

            Assert.AreEqual(b.At(5,5), board[5,5]);
            Assert.AreNotSame(b.Board, board);
        }

        [TestMethod]
        public void TestTake()
        {
            var b  = new BoggleBoard(10);
            var bt = b.Take(5,5);

            Assert.AreNotSame(b, bt);
            Assert.AreEqual(BoggleBoard.Empty, bt.At(5,5));
            Assert.AreNotEqual(b.At(5,5), bt.At(5,5));
        }

        [TestMethod]
        public void TestGet()
        {
            var b = new BoggleBoard(10);
            var c = b.Board[5, 5];

            b.Board[5, 5] = BoggleBoard.Empty;

            Assert.AreEqual(b.Board[5,5], c);
        }
    }
}

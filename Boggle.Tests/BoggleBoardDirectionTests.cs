﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibBoggle.Tests
{
    [TestClass()]
    public class BoggleBoardDirectionTests
    {
        [TestMethod()]
        public void GetPotentialDirectionsTest()
        {
            var boggle = new BoggleBoard(10);

            //Cant go up or left
            Assert.AreEqual(3, boggle.GetPotentialDirections(0, 0).Count);
            //can't go right or down
            Assert.AreEqual(3, boggle.GetPotentialDirections(9, 9).Count);
            //can't go up
            Assert.AreEqual(5, boggle.GetPotentialDirections(0, 5).Count);
            //can't go right
            Assert.AreEqual(5, boggle.GetPotentialDirections(5, 0).Count);

            //Can't go up or Right
            Assert.AreEqual(3, boggle.GetPotentialDirections(0, 9).Count);
            //Can't go left or down
            Assert.AreEqual(3, boggle.GetPotentialDirections(9, 0).Count);

            //omni
            Assert.AreEqual(8, boggle.GetPotentialDirections(5, 5).Count);
            
        }

        [TestMethod()]
        public void GetPotentialDirectionsEmpty()
        {
            var boggle = new BoggleBoard(10);

            //All Empty - take everything surrounding 5/5
            var b = boggle.Take(5 - 1, 5).Take(5 - 1, 5 + 1).Take(5, 5 + 1).Take(5 + 1, 5 + 1)
                .Take(5 + 1, 5).Take(5 + 1, 5 - 1).Take(5, 5 - 1).Take(5 - 1, 5 - 1);
            Assert.AreEqual(0, b.GetPotentialDirections(5,5).Count);

        }
    }
}
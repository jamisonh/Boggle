# Boggle

Web API 2.0 API and library to find words on a boggle board.  Also includes API endpoints for the dictionary, for word validation.

Swagger UI (via Swashbuckle) is included for interacting with the API.

## Endpoints

|HTTP Method | Endpoint  | Query params  | Example   | Description 
|:-:|---|---|---|---|
| GET  | api/boggle/generate  | *Required* boardSize (int)<br/> *optional* minWordLength (int)<br/> *optional* concurrent (bool)  | /api/boggle/generate?boardSize=4&minWordLength=4&concurrent=true  | Generates a randomly filled board with values and finds all possible words from it.
| POST  | /api/boggle  | *optional* minWordLength (int)<br/> *optional* concurrent (bool)  | api/boggle?minWordLength=5&concurrent=true  | Receives the input boggle board inside the json post body and finds all the possible words on it. |
| GET  | api/words | prefix  | /api/words?prefix=supercali  | returns all entries that start with the input prefix
| GET  | api/words/{word}   | NONE  | /api/words/supercretaceous  | returns all entries that match the given word
| GET  | api/words/{word}/exists  | NONE  | /api/words/supercilium/exists  | returns a boolean to indicate whether the word exists in the dictionary or not.
| GET  | /api/words/exists  | prefix  | api/words/exists?prefix=exi  | returns a boolean to indicate whether the prefix exists in the dictionary or not.

## Running

You can run this solution in Visual Studio, in Docker For Windows with a prebuilt Docker image, or by building the docker image.  Instructions for doing all 3 are below.

> This application was built using Visual Studio 2015.

Application is setup for the default route (`/`) to be redirected to Swagger UI for interacting with the API.

### Visual Studio (Debug)

  1. Open the solution in visual studio
  1. Set the startup project as `Boggle` (should be already)
  1. Restore nuget packages (if necessary)
  1. Run in debug mode
     1. Browser should open to the default route `/` and get redirected to swagger UI.

### Docker For Windows

You'll need to install Docker on Windows Server 2016 or Windows 10 and "Switch to Windows containers" in it's context menu.

Also of note - Windows Containers are very large compared to linux - Apline Linux is 6MB, Ubuntu is around 300MB, while a stripped down windows server core with IIS and asp.net installed is over 7 GB.

> Docker on windows currently has a bug that prevents you from accessing the running container on `localhost`.  This requires you to run a `docker inspect {container ID}` and use the IP Address assigned by docker (probably 172.something)

#### Running Pre-built Docker Image

Running the following command in PowerShell (from any directory) will start the container and print the assigned IP Address:

> Warning: The following will take awhile...

``` powershell
docker inspect -f "{{ .NetworkSettings.Networks.nat.IPAddress }}" $(docker run --rm -d -p 8080:80 jamisonhyatt/boggle)
```

Navigate to the output IP address to interact with Swagger UI

##### Building, Compiling and Running in Docker

These steps will mount the source code into an ephemeral build container that contains `MSBuild` and `nuget`.  Once running, the binaries will be built from the source code and copied into another Windows Server image, with both asp.net and IIS installed.

> Warning - This will take longer than the prebuilt image, as it will need to download both the image used to compile and the eventual image for the application to be run on.

In PowerShell from the solution directory run...

``` powershell
docker build -t boggle .
docker inspect -f "{{ .NetworkSettings.Networks.nat.IPAddress }}" $(docker run --rm -d -p 8080:80 boggle)
```

Navigate to the output IP address to interact with Swagger UI
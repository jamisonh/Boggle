﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LibBoggle
{
    public class BoggleBoard
    {
        private static readonly Random Rand = new Random();
        private static readonly char[] ValidBoardCharacters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        public const char Empty = char.MinValue;

        public uint Size { get; }

        private readonly char[,] _board;
        public char[,] Board => _board.Clone() as char[,];
        
        /// <summary>
        /// Generates a new square BoggleBoard with random letters throughout.
        /// </summary>
        /// <param name="size">The size of the square BoggleBoard to be created.</param>
        public BoggleBoard(uint size)
        {
            Size = size;
            _board = GenerateBoard(size);
        }

        /// <summary>
        /// Genereates a new square BoggleBoard from a given 2d char array
        /// </summary>
        /// <param name="board">The square 2d board that should be used in this new BoggleBoard</param>
        private BoggleBoard(char[,] board)
        {
            Size = (uint)board.GetLength(0);
            if (Size != board.GetLength(1))
            {
                throw new ArgumentException("Board is not a square");
            }
            _board = board;

        }

        /// <summary>
        /// Creates a new BoggleBoard for the input 2d Char array after validating the input
        /// </summary>
        /// <param name="board">The board to use for this BoggleBoard</param>
        /// <returns>A BoggleBoard with the input 2d char array</returns>
        public static BoggleBoard FromBoard(char[,] board)
        {
            return new BoggleBoard(ValidateBoard(board));
        }
        
        /// <summary>
        /// Generates a new fully randomized board
        /// </summary>
        /// <param name="size">The size of the board (square) to create </param>
        /// <returns></returns>
        private static char[,] GenerateBoard(uint size)
        {
            var board = new char[size, size];

            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    board[i, j] = ValidBoardCharacters[Rand.Next(0, 26)];
                }
            }

            return board;
        }

        /// <summary>
        /// Returns the charact at specific row/column
        /// </summary>
        public char At(uint row, uint col)
        {
            return Board[row, col];
        }

        /// <summary>
        /// Takes the current row/col value from a copy of the current board, and returns it
        /// </summary>
        /// <returns>a copy of the current BoggleBoard's board less the value in the row/col </returns>
        private char[,] TakeVal(uint row, uint col)
        {
            var b = Board;
            b[row, col] = Empty;
            return b;
        }

        /// <summary>
        /// Removes the value at the row/column and returns a new BoggleBoard
        /// </summary>
        /// <returns>Clone of the boggleboard less the removed value.</returns>
        public BoggleBoard Take(uint row, uint col)
        {
            return new BoggleBoard(TakeVal(row, col));
        }
        
        /// <summary>
        /// Evaluates the input position on the board, and compiles a list of valid* coordinates
        ///   *Valid meaning non-negative, non Empty coordinates exactly 1 position away.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns>List of valid, non-empty Coordinates on the boggle board</returns>
        public List<Coordinate> GetPotentialDirections(uint row, uint col)
        {
            //8 possible directions, moving clockwise
            var directions = new List<Coordinate>();

            //up
            if (row != 0 && At(row - 1, col) != Empty)
            {
                directions.Add(new Coordinate(row - 1, col));
            }

            //diagonal right up
            if (row != 0 && col != Size - 1 && At(row - 1, col + 1) != Empty)
            {
                directions.Add(new Coordinate(row - 1, col + 1));
            }

            //right
            if (col != Size - 1 && At(row, col + 1) != Empty)
            {
                directions.Add(new Coordinate(row, col + 1));
            }

            //diagonal right down
            if (row != Size - 1 && col != Size - 1 && At(row + 1, col + 1) != Empty)
            {
                directions.Add(new Coordinate(row + 1, col + 1));
            }

            //down
            if (row != Size - 1 && At(row + 1, col) != Empty)
            {
                directions.Add(new Coordinate(row + 1, col));
            }

            //diagonal left down
            if (row != Size - 1 && col != 0 && At(row + 1, col - 1) != Empty)
            {
                directions.Add(new Coordinate(row + 1, col - 1));
            }

            //diagonal left
            if (col != 0 && At(row, col - 1) != Empty)
            {
                directions.Add(new Coordinate(row, col - 1));
            }

            //diagonal left up
            if (row != 0 && col != 0 && At(row - 1, col - 1) != Empty)
            {
                directions.Add(new Coordinate(row - 1, col - 1));
            }
            return directions;
        }

        /// <summary>
        /// Validates the given board, by verifying it is in fact a square and all values within
        /// are valid characters
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        private static char[,] ValidateBoard(char[,] board)
        {
            if (board == null)
            {
                throw new ArgumentException("Attempted to validate input board, but received a null board.");
            }
            var size = (uint)board.GetLength(0);
            if (size != board.GetLength(1))
            {
                throw new ArgumentException("Board is not a square");
            }

            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    var letter = char.ToLower(board[i, j]);
                    if (letter == Empty)
                    {
                        throw new ArgumentException($"Input at row {i}, column {j} is empty");
                    }
                    if (!ValidBoardCharacters.Contains(letter))
                    {
                        throw new ArgumentException($"Invalid board character: {letter} at row {i}, column {j}");
                    }

                    board[i, j] = letter;
                }
            }

            return board;
        }


    }

    /// <summary>
    /// Represents row and column coordinates on a valid board.  
    /// </summary>
    public class Coordinate
    {
        public uint Row { get; }
        public uint Col { get; }

        public Coordinate(uint row, uint col)
        {
            Row = row;
            Col = col;
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using System.Web;

namespace LibBoggle.Data
{
    public interface IBoggleDictionary
    {
        /// <summary>
        /// Searches for a single word in the dictionary, and returns all entries for that word.
        /// </summary>
        /// <param name="word">The word to search for.</param>
        /// <returns>A list of entries matching the input word; returns an 
        /// empty list if the word was not found</returns>
        Task<List<DictionaryEntry>> GetEntries(string word);

        /// <summary>
        /// Searches the dictionary by a prefix, and returns all entries that match that prefix.
        /// </summary>
        /// <param name="prefix">The prefix to search for</param>
        /// <returns>A list of entries matching the input prefix; returns an 
        /// empty list if the prefix was not found</returns>
        Task<List<DictionaryEntry>> GetEntriesByPrefix(string prefix);

        /// <summary>
        /// Searches the dictionary for a given input word, and returns whether it exists or not.
        /// </summary>
        /// <param name="word">The word to check for</param>
        /// <returns>True (the word exists) or False (the word does not exist) </returns>
        Task<bool> IsWord(string word);

        /// <summary>
        /// Searches the dictionary for the given prefix, and returns a boolean that indicates
        /// if any words in the dictionary begin with that prefix.
        /// </summary>
        /// <param name="prefix">The prefix to search for</param>
        /// <returns>True (words with the prefix exist) or False (no words with the prefix exist)</returns>
        Task<bool> IsPotentialWord(string prefix);
    }

    /// <summary>
    /// Represents a single instance of a boggle dictionary
    /// While it is not necessary, the intent here is to use this object as a Singleton.
    /// Typically, this would be enforced via Dependency Injection.
    /// </summary>
    public class BoggleDictionary : IBoggleDictionary
    {
        private readonly string _connectionString;

        public BoggleDictionary()
        {

         
            _connectionString = new SQLiteConnectionStringBuilder
            {
                DataSource = HttpRuntime.AppDomainAppPath + "\\dictionary.sqlite",
                Pooling = true,
                ReadOnly = true
                
            }.ConnectionString;
            
        }

        public async Task<List<DictionaryEntry>> GetEntries(string word)
        {
            var entries = new List<DictionaryEntry>();
            const string query = "select word, word_type, definition from entries where word = @word";
            using (var conn = new SQLiteConnection(_connectionString))
            {
                await conn.OpenAsync();
                using (var cmd = new SQLiteCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@word", word);
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while(await reader.ReadAsync())
                        {
                            entries.Add(new DictionaryEntry()
                            {
                                Word = reader.GetString(0),
                                WordType = reader.GetString(1),
                                Definition = reader.GetString(2)
                            });
                        }

                        return entries;
                    }
                }
                
            }
        }

        public async Task<List<DictionaryEntry>> GetEntriesByPrefix(string prefix)
        {
            var entries = new List<DictionaryEntry>();
            const string query = "select word, word_type, definition from entries where word like @prefix || '%'";
            using (var conn = new SQLiteConnection(_connectionString))
            {
                await conn.OpenAsync();
                using (var cmd = new SQLiteCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@prefix", prefix);
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            entries.Add(new DictionaryEntry()
                            {
                                Word = reader.GetString(0),
                                WordType = reader.GetString(1),
                                Definition = reader.GetString(2)
                            });
                        }

                        return entries;
                    }
                }

            }
        }

        public async Task<bool> IsWord(string word)
        {
            const string query = @"SELECT EXISTS(SELECT 1 FROM entries WHERE word=@word LIMIT 1);";
            using (var conn = new SQLiteConnection(_connectionString))
            {
                await conn.OpenAsync();
                using (var cmd = new SQLiteCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@word", word);
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        return reader.GetInt16(0) == 1;
                    }
                }
            }
        }

        public async Task<bool> IsPotentialWord(string prefix)
        {
            const string query = @"SELECT EXISTS(SELECT 1 FROM entries WHERE word like @prefix || '%' LIMIT 1);";
            using (var conn = new SQLiteConnection(_connectionString))
            {
                await conn.OpenAsync();
                using (var cmd = new SQLiteCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@prefix", prefix);
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        return reader.GetInt16(0) == 1;
                    }
                }
            }
        }

    }
}
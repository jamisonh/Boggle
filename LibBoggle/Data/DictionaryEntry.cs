﻿using System.Runtime.Serialization;

namespace LibBoggle.Data
{
    [DataContract]
    public class DictionaryEntry
    {
        [DataMember]
        public string Word { get; set; }

        [DataMember]
        public string WordType { get; set; }

        [DataMember]
        public string Definition { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LibBoggle.Data;

namespace LibBoggle
{
    public class BoggleWordFinder
    {
        private readonly HashSet<string> _foundWords = new HashSet<string>();
        private readonly HashSet<string> _nonexistentPrefixes = new HashSet<string>();
        private readonly IBoggleDictionary _boggleDictionary;
        private uint _minWordLength;

        /// <summary>
        /// Private constructor to ensure this object is ephemeral, and only used by GetWords.
        /// </summary>
        /// <param name="boggleDictionary"></param>
        private BoggleWordFinder(IBoggleDictionary boggleDictionary)
        {
            _boggleDictionary = boggleDictionary;
        }
        
        /// <summary>
        /// Traverses a given BoggleBoard, and compares possible prefixes with the provided dictionary.
        /// 
        /// This Method, while always asynchronous, can optionally start a concurrent Task execution for every individual position on the board, which
        /// creates a 3-4x reduction in runtime (on a quad core PC), at the cost of hogging cpu time for the rest of the 'server'.
        /// 
        /// This concurrent options something you would let people abuse - or, if you did...you'd want to implement some heavy scaling to ensure capacity.
        /// 
        /// </summary>
        /// <param name="board">The BoggleBoard to get words from</param>
        /// <param name="boggleDictionary">the dictionary to use for word and prefix validation</param>
        /// <param name="minWordLength">The minimum length a string needs to be, before it is considered a word.</param>
        /// <param name="concurrent">boolean Whether to hog server CPU time to process each starting point on the board concurrently.</param>
        /// <returns></returns>
        public static async Task<HashSet<string>> GetWords(BoggleBoard board, IBoggleDictionary boggleDictionary, uint minWordLength = 3, bool concurrent = false)
        {
            var wordFinder = new BoggleWordFinder(boggleDictionary)
            {
                _minWordLength = minWordLength
            };
            // Store all our asynchronous tasks to avoid waiting for each one.
            var tasks = new List<Task>();

            // Traverse the entire board, and for each position, start an async task
            // that traversis all 8 directions from that starting point
            for (uint row = 0; row < board.Size; row++)
            {
                for (uint column = 0; column < board.Size; column++)
                {
                    // Don't pass row/column refs into the async tasks, as loop will 
                    // update those values underneath before the task is started.
                    var taskRow = row;
                    var taskColumn = column;
                    

                    if (concurrent)
                    {   //Immediately starts a thread to run this task.
                        tasks.Add(Task.Factory.StartNew(() => wordFinder.FindWords("", taskRow, taskColumn, board)));
                    }
                    else
                    {   //queues the task, but does not ask for a new thread from the CLR for immediate execution.
                        //This is mostly single threaded, but still concurrent.
                        tasks.Add(wordFinder.FindWords("", taskRow, taskColumn, board));
                    }
                }
            }
            // Wait for all the tasks to complete before finishing.
            await Task.WhenAll(tasks);

            return wordFinder._foundWords;
        }

        /// <summary>
        /// Recursive Functon that checks the given position on the provided boggleboard, and appends the character found to the input prefix.
        /// 
        /// Each recursive function will be called with a new BoggleBoard less value appended to the prefix.
        /// </summary>
        /// <param name="prefix">The prefix of a potential new word to append to</param>
        /// <param name="row">The row of the character to append to prefix</param>
        /// <param name="col">The column of the character to append to the prefix</param>
        /// <param name="boggle">The running boggleboard to use</param>
        /// <returns></returns>
        private async Task FindWords(string prefix, uint row, uint col, BoggleBoard boggle)
        {
            // Store the potential word
            var potentialWord = prefix + boggle.Board[row, col];

            // Exit early if we've already found this prefix and we can save a DB call
            if (_nonexistentPrefixes.Contains(potentialWord))
                return;

            //If the word is valid length, and it's not already a valid word, and it's a word according to the dictionary...
            if ( potentialWord.Length >= _minWordLength && !_foundWords.Contains(potentialWord) &&  await _boggleDictionary.IsWord(potentialWord))
            {   // Add it to the list of discovered words.
                _foundWords.Add(potentialWord);   
            }
            // Otherwise, if it's not an existing word, and it's not even a potential word....
            else if (!_foundWords.Contains(potentialWord) && !await _boggleDictionary.IsPotentialWord(potentialWord))
            {
                //Add it to the nonexistent prefixes and return early - don't continue search for words past where we know there can be none.
                _nonexistentPrefixes.Add(potentialWord);
                return;
            }
            // Remove this value off the board, so we don't re-process it.
            boggle = boggle.Take(row, col);

            // For each potential direction, recursively call FindWords and continue traversing in all directions.
            foreach (var dir in boggle.GetPotentialDirections(row, col))
            {
                await FindWords(potentialWord, dir.Row, dir.Col, boggle);
            }
        }

    }
}
